def gcd(x,y):
  while y:
    x, y = y, x%y
  return x

def mixed_fraction(s):
  n, d = [int(x) for x in s.split('/')]

  if (d == 0):
    return 1/d

  if (n == 0):
    return '0'

  w = int(n / (d + 0.0))
  fn = abs(n) % abs(d)
  fd = abs(d)

  if (fn == 0):
    return '{0}'.format(w)

  r = gcd(max(fn, fd), min(fn, fd))

  if (w == 0):
    if (n < 0):
      fn *= -1

    if (d < 0):
      fn *= -1

    return '{0}/{1}'.format((fn/r), (fd/r))

  return "{0} {1}/{2}".format(w, (fn/r), (fd/r))

solution = mixed_fraction('-10/7')
compare_val = '-1 3/7'

if solution == compare_val:
  print("successful: {0} == {1}".format(solution, compare_val))
else:
  print("unsuccessful: {0} != {1}".format(solution, compare_val))
